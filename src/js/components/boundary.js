import { Body, Bodies, World } from 'matter-js';

export default class Boundary {
	constructor(x, y, w, h, r, fill, engine) {
		this.create = () => {
			const boundary = Bodies.rectangle(x, y, w, h, {
				density: 0.0001,
				restitution: 0.1,
				friction: 1,
				isStatic: true,
				render: {
					fillStyle: `${fill}`
				}
			})
			Body.rotate(boundary, r);
			// Add it to the scene
			World.add(engine.world, boundary);
		}
	}
}