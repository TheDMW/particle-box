import { Bodies, World } from 'matter-js';

export default class Ball {
	constructor(x, y, r, fill, engine) {
		this.create = () => {
			let ball = Bodies.circle(x, y, r, {
				density: 0.0001,
				restitution: 1,
				friction: 0.01,
				render: {
					fillStyle: `${fill}`
				}
			})

			// Add it to the scene
			World.add(engine.world, ball);
		}
	}
}