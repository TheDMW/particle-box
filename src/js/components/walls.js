import { Bodies, World } from 'matter-js';

export default class Walls {
	constructor(w, h, fill, engine) {
		this.create = () => {
            const options = {
                isStatic: true,
                render: {
                    fillStyle: `${fill}`
                }
            }

            // Define boundaries
            const roof = Bodies.rectangle(w / 2, 0, w, 30, options)
            const floor = Bodies.rectangle(w / 2, h, w, 30, options)
            const left = Bodies.rectangle(0, h / 2, 30, h, options)
            const right = Bodies.rectangle(w, h / 2, 30, h, options)

			// Add it to the scene
			World.add(engine.world, [roof, floor, left, right]);
		}
	}
}