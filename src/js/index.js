import { Engine, Render, World, Mouse, MouseConstraint } from 'matter-js';
import Colors from 'nice-color-palettes';

import Ball from './components/ball';
import Boundary from './components/boundary';
import Walls from './components/walls';

import '../scss/styles.scss';

// Create arrays
let colorArr = Colors;

// Create engine
const engine = Engine.create();
const world = engine.world;

// Empty variables
let w;
let h;
let render;

const setup = () => {
	calcSize();

	//Create renderer
	render = Render.create({
		element: document.body,
		engine: engine,
		options: {
			width: w,
			height: h,
			wireframes: false,
			background: `${randomColors()}`
		}
	});

	// Run the engine
	Engine.run(engine);

	// Run the renderer
	Render.run(render);

	// Start mouse interactivity
	startMouse();

	// Render the scene to the canvas
	renderScene();
}

// Calculate screen size
const calcSize = () => {
	w = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
	h = Math.max(document.documentElement.clientHeight, window.innerHeight || 0);
}

const randomNumber = (min, max) => {
	const n = Math.floor(Math.random() * max) + min;

	return n;
}

// Generate random colors
const randomColors = () => {
	// Pick random number from entire length of array
	const r = randomNumber(0, colorArr.length);

	// Choose set of colors from array
	let arr = colorArr[r];

	// Pick random number
	const i = [randomNumber(0, 4)];

	// Set random color to this variable
	const bg = arr[i];

	// Delete this color from array
	arr.splice(i, 1);

	// Fill colorArr with remaining colors
	colorArr = arr;

	// Return background color
	return bg;
}

const startMouse = () => {
	const mouse = Mouse.create(render.canvas),
		mouseConstraint = MouseConstraint.create(engine, {
		mouse: mouse,
		constraint: {
			stiffness: 0.2,
			render: {
				visible: false
			}
		}
	});

	World.add(world, mouseConstraint);

	// keep the mouse in sync with rendering
	render.mouse = mouse;
}

// Generate boundaries
const generateBoundaries = () => {
	let r;

	// Random amount of boundaries
	if (w > 500) {
		r = randomNumber(20, 60);
	} else {
		r = randomNumber(0, 45);
	}

	// Create length, x, y and apply it to a new boundary for this random amount
	for (let i = 0; i < r; i++) {
		const l = randomNumber(50,200);
		const x = randomNumber(30, w - (l / 2));
		const y = randomNumber(0, h);
		const r = randomNumber(0, 360);

		const bound = new Boundary(x, y, l, 30, r, `${colorArr[randomNumber(0,4)]}`, engine);

		// Render the boundary
		bound.create();
	}
}

// Generate walls
const generateWalls = () => {
	const walls = new Walls(w, h, `${colorArr[randomNumber(0,4)]}`, engine);

	return(walls.create());
}

// Generate balls
const generateBalls = () => {
	const x = randomNumber(50, (w - 50));
	const y = 50;
	for(let i = 0; i < 200; i++) {
		(function (i) {
			setTimeout(function () {
				const r = randomNumber(5, 13);
				const newBall = new Ball(30 + (w / 200) * i, y, r, `${colorArr[randomNumber(0,4)]}`, engine);
				newBall.create();
			}, 10 * i)
		})(i);
	}
}

// Render the scene
const renderScene = () => {
	generateBoundaries();
	generateWalls();
	generateBalls();
}

// Run setup
setup();
